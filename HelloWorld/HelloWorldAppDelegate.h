//
//  HelloWorldAppDelegate.h
//  HelloWorld
//
//  Created by Mauro Mazzieri on 23/01/13.
//  Copyright (c) 2013 Informatica R&D srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
