//
//  HelloWorldViewController.h
//  HelloWorld
//
//  Created by Mauro Mazzieri on 23/01/13.
//  Copyright (c) 2013 Informatica R&D srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldViewController : UIViewController <UITextFieldDelegate>
@property (copy, nonatomic) NSString *userName;
@end
