//
//  main.m
//  HelloWorld
//
//  Created by Mauro Mazzieri on 23/01/13.
//  Copyright (c) 2013 Informatica R&D srl. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HelloWorldAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HelloWorldAppDelegate class]));
    }
}
